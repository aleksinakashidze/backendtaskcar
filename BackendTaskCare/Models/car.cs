﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendTaskCare.Models
{
    public class car
    {
        public Model Models { get; set; }
        public int Year { get; set; }
        public int MyProperty { get; set; }
        public string About { get; set; }
        public Guid Picture { get; set; }
        public Fecility Fecilitys { get; set; }
        public int Price { get; set; }
        public int PriceUSD { get; set; }
        public int PriceEUR { get; set; }
    }
    public enum Model
    {
        Audi,
        BMV,
        Mersedes,
        Toyota,
        Mitsubishi

    }
    public class Fecility
    {
        public Name ID { get; set; }
        public string Name { get; set; }
        public bool Visible { get; set; }
    }
    public enum Name
        {
        ABS,
        elect_shush_amwev,
        luqi,
        Bluetooth,
        signalizacia,
        parkingkontroli,
        navigacia,
        bortkompiuteri,
        multi_sawe
    }

}
